title: "Kvóty zatím nefungují, země přijaly jen osminu plánovaných azylantů.
Podívejte se, kam by sami chtěli"
perex: ""
authors: ["Jan Boček"]
published: "19. června 2017"
coverimg: https://www.irozhlas.cz/sites/default/files/images/03837284.jpeg
coverimg_note: "Uprchlíci z Mosulu | <a href='https://www.irozhlas.cz/admin/zpravy/media/image/5508424/'>iRozhlas</a>"
styles: []
libraries: ["https://unpkg.com/jquery@3.2.1", "https://code.highcharts.com/highcharts.js"]
options: "" #wide
---

**Před dvěma roky schválila Evropská unie pravidla, jimiž během migrační krize chtěla odlehčit nejhůř postiženým zemím - Řecku a Itálii. Místo 160 tisíc běženců si ovšem ostatní členové osmadvacítky dosud převzaly jen 20 tisíc. Desítka zemí pak nepřijala žádné nebo téměř žádné.**

Evropská komise od úterý minulého šetří Česko, které odmítá přijímat azylanty ze Středomoří. Od spuštění systému Češi přijali místo 2691, které určují kvóty, jen dvanáct lidí.

„Jsme připraveni hájit pozici ČR, doufám, že ji obhájíme,“ řekl k tomu předseda vlády Bohuslav Sobotka. „Systém přerozdělování je pobídka pro nelegální migraci,“ dodal.

Stejné šetření se týká také Polska a Maďarska. Podle <a href="https://ec.europa.eu/home-affairs/sites/homeaffairs/files/what-we-do/policies/european-agenda-migration/press-material/docs/state_of_play_-_relocation_en.pdf">aktuálních statistik Evropské komise</a> je ovšem zemí, které se kvótám brání, víc. Žádné nebo téměř žádné azylanty dosud nepřijaly kromě Česka, Maďarska a Polska také Bulharsko, Chorvatsko, Rakousko a Slovensko.

Další dvě země – Spojené království a Dánsko – kvóty odmítly na základě výjimek, které si s unií vyjednaly v minulosti. Obě země se nicméně zavázaly přijmout řádově tisíce středomořských azylantů mimo systém kvót.

<wide><div id="kvoty"></div></wide>

<h2>Česko, Polsko, Maďarsko: tři cesty za hranu</h2>

„Česko, Polsko a Maďarsko jsou země, kde ohledně kvót dochází k evidentnímu porušování evropských pravidel,“ vysvětluje ředitel Organizace pro pomoc uprchlíkům Martin Rozumek, proč se Evropská unie zaměřila právě na ně. „Ostatní se přijímání azylantů nebrání nebo už pro ně něco dělají. Třeba Rakousko přijalo v roce 2015 nejvíc azylantů na jednoho obyvatele z celé Evropy. Slovensko zase provozuje tranzitní centrum, přes nějž míří uprchlíci dál do Evropy.“

Každá ze tří šetřených zemí se za hranu dostala jinou cestou. Češi sice kvóty vytrvale odmítají už od prvního hlasování v Radě ministrů, nicméně v roce 2015 slíbili dobrovolně přijmout patnáct set azylantů na základě <a href="http://www.mvcr.cz/migrace/clanek/migrace-casto-kladene-dotazy-casto-kladene-dotazy.aspx?q=Y2hudW09Mw%3D%3D">vlastních kritérií</a>. Vláda ale neplní ani svůj závazek.

Poláci nejprve hlasovali pro přijetí kvót. Krátce nato ovšem v zemi převzala moc konzervativní vláda premiérky Beaty Szydłové, která kvóty naopak rázně odmítla.

Třetí prošetřovaná země - Maďarsko - původně neměla středomořské azylanty přijímat vůbec. Naopak, jelikož na ně dopadla uprchlická vlna podobně jako na Řecko a Itálii, měli Maďaři azylanty posílat do ostatních evropských zemí. Plán, kdy by z Maďarska odešlo přes 50 000 azylantů, ale vláda odmítla. „Podle nás bude nemožné udržet dejme tomu na Slovensku lidi, kteří chtějí do Německa,“ opakovala tehdy vláda premiéra Viktora Orbána nejčastější argument proti kvótám.

„Maďarsko problém s azylanty vyřešilo po svém,“ vysvětluje Rozumek. „Unijní pravidla vláda odmítla. Azylantům pak připravila tak šílené podmínky, že zemi opustili. Tím ovšem problém pouze nezákonně přehodila na ty země, kam se uprchlíci přesunuli.“

Ještě v roce 2015 Maďaři napadli systém kvót u Soudního dvora Evropské unie (výsledek se očekává na konci letošního roku) a loni o nich uspořádali referendum. V něm sice jednoznačně zvítězili odpůrci kvót, ale kvůli malé účasti nebylo platné.

<h2>Vlády si diktují</h2>

Ani státům, které azylanty přijímají, se ale nedaří kvóty naplnit. 'Odškrtnuto' mají pouze Malta a Lichtenštejnsko, z lidnatějších zemí jsou na dobré cestě Finsko a Norsko, které se ke kvótám připojilo, ačkoliv není členem EU.

„Množství těch, kteří jsou v Řecku nebo Itálii připraveni na přesun, je mnohem nižší než původní cíle,“ řekl v dubnu novinářům eurokomisař pro migraci Dimitris Avramopoulos. Hlavní problém je podle něj v tom, že řecké a italské úřady nejsou schopné azylanty dostatečně rychle identifikovat a vyloučit bezpečnostní riziko.

Jak líčí britský EUobserver, každá země má navíc specifické požadavky. „Bulharsko například nepřijme nikoho z Eritreje,“ píše. „Slovensko přijímá jen samotné ženy s dětmi a s cestovními doklady, Estonsko a Irsko nepřijmou nikoho z Itálie.“

„Je pravda, že podmínky, které si některé země kladou, můžou přejímání azylantů zpomalit,“ tvrdí Martin Rozumek z Organizace pro pomoc uprchlíkům. „Ale pokud takové podmínky myslí upřímně, je to v pořádku. V Kanadě přijali 30 tisíc Syřanů s tím, že půjde o kompletní rodiny. Česko by stejně tak mohlo přijmout třeba několik stovek opuštěných dětí, které zjevně nejsou bezpečnostním rizikem. Problém je, že řada zemí si stanoví podmínky jen jako záminku, aby nemusely přijmout nikoho.“

<h2>Roky 2015 a 2016 byly rekordní: přes milion azylantů každý rok</h2>

Kvóty řeší pouze menší část azylantů; podle <a href="http://popstats.unhcr.org/en/asylum_seekers_monthly">dat Agentury OSN pro uprchlíky</a> zamířilo v roce 2015 do Evropské unie 1,3 milionu azylantů, o rok později 1,2 milionu. Z toho přibližně polovina se žádostí o azyl uspěla. Některé země – například Německo – zažily srovnatelnou vlnu na začátku devadesátých let, kdy před válkou do unie prchaly oběti války v zemích bývalé Jugoslávie.

Přesto jde z pohledu Evropy o rekordní roky: v roce 1992, kdy byla vlna balkánských uprchlíků nejsilnější, zamířila do zemí unie přibližně polovina azylantů, co loni.

Stejná data ukazují také to, koho jednotlivé evropské země přitahují. V Německu nebo Švédsku například žádají o azyl nejčastěji Syřané, Iráčané a Afghánci, v Itálii jsou to Nigerijci a ve Francii uprchlíci z řady afrických zemí v čele s Kongem.
<wide>
<div id="selector" onchange="checkSelect()">
    <select id="vyberZeme">
        <option value="belgie">Belgie</option>
        <option value="bulharsko">Bulharsko</option>
        <option value="cesko" selected="selected">Česko</option>
        <option value="dansko">Dánsko</option>
        <option value="finsko">Finsko</option>
        <option value="francie">Francie</option>
        <option value="irsko">Irsko</option>
        <option value="italie">Itálie</option>
        <option value="kypr">Kypr</option>
        <option value="lucembursko">Lucembursko</option>
        <option value="madarsko">Maďarsko</option>
        <option value="malta">Malta</option>
        <option value="nemecko">Německo</option>
        <option value="nizozemsko">Nizozemsko</option>
        <option value="norsko">Norsko</option>
        <option value="polsko">Polsko</option>
        <option value="rakousko">Rakousko</option>
        <option value="rumunsko">Rumunsko</option>
        <option value="recko">Řecko</option>
        <option value="slovensko">Slovensko</option>
        <option value="slovinsko">Slovinsko</option>
        <option value="uk">Spojené království</option>
        <option value="srbsko">Srbsko a Kosovo</option>
        <option value="spanelsko">Španělsko</option>
        <option value="svedsko">Švédsko</option>
        <option value="svycarsko">Švýcarsko</option>
    </select>
</div>

<div id="container" style="height: 600px;"></div>
<i>Graf ukazuje země EU, které od roku 1999 registrovaly nejméně 10 tisíc azylantů</i>
</wide>